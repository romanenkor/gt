#include <iostream>
#include <tuple>
#include <cassert>
#include <unordered_set>
#include <array>
#include <vector>
#include <string_view>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <string_view>
#include <algorithm>
#include <memory>
#include <thread>
#include <unordered_map>
#include <fcntl.h>
#include <unistd.h>
#include <bitset>
#include <ftl/container.hpp>
#include <ftl/benchmark.hpp>
#include <ftl/bits.hpp>
#include <ftl/args.hpp>

auto buffer = static_cast<const char*>(nullptr);
auto buffer_size = 0;

auto token_count = 0;
auto expression_count = 0;
auto tid = 0;
auto eid = 0;
auto flags = 0;

enum class character_type : u8 {
    whitespace, letter, special
};

enum class token_type : u8 {
    whitespace, identifier, special,
    keyword, import, comment,
    arrow, expression
};

struct token {
    int finish;
    int start;
    token_type type;
};

struct expression {
    int start;
    int finish;
    int next;
};

std::array<expression, 4096 * 100> expressions;
std::array<token, 4096 * 100> tokens;

static std::vector<character_type> types = []() -> auto {
    std::vector<character_type> result(128);
    for (auto c = " \r\n\t\v\f"; *c != '\0'; ++c) {
        result[*c] = character_type::whitespace;
    }
    for (auto c = "=+-*/<>{}()"; *c != '\0'; ++c) {
        result[*c] = character_type::special;
    }
    for (auto c = 'a'; c <= 'z'; ++c) {
        result[c] = character_type::letter;
    }
    for (auto c = 'A'; c <= 'Z'; ++c) {
        result[c] = character_type::letter;
    }
    for (auto c = '0'; c <= '9'; ++c) {
        result[c] = character_type::letter;
    }
    return result;
}();

static struct {
    unsigned i1 : 8;
    unsigned i2 : 8;
    unsigned i3 : 8;
    unsigned t : 8;
} dtable[256]{};

int dinit = []() {
    using namespace ftl;
    for (auto k = 0; k < 256; ++k) {
        auto x1 = (u8)k;
        auto i1 = ctz(x1);
        dtable[k].i1 = std::min(i1 + 1, 4);
        auto x2 = x1 & ~(1 << i1);
        auto i2 = ctz(x2);
        dtable[k].i2 = std::min(i2 + 1, 4);
        auto x3 = x2 & ~(1 << i2);
        auto i3 = ctz(x3);
        dtable[k].i3 = std::min(i3 + 1, 4);
        dtable[k].t = std::min(popcnt((u32)k), (u32)3);
    }
    return 0;
}();

void lex() {
    auto text = buffer;
#if 0
    auto mod = sizeof(tokens)/sizeof(*tokens);
    auto i = 1;
    auto t = 0;
    auto stype = tokens[0].type = types[text[0]];
    tokens[0].start = 0;

    for (; i < buffer_size; ++i) {
        auto type = types[text[i]];
        if (type != stype)
        {
            tokens[t].finish = i;
            t += 1; s %= mod;

            tokens[t].start = i;
            tokens[t].type = stype = type;
        }
    }
    //tokens[t1].finish = i;
#endif
#if 0
    auto i = 1;
    auto t = 0;
    auto stype = tokens[0].type = types[text[0]];
    tokens[0].start = 0;

    for (; i < buffer_size; ++i) {
        auto s0 = text[i-1];
        auto s1 = text[i];
        auto t0 = types[s0];
        auto t1 = types[s1];
        auto delta = ((t1 - t0) | (t0 - t1)) >> 31;
        tokens[t].type = t0;
        tokens[t].finish = i;
        tokens[t + 1].start = i;
        t += delta;
    }
    //tokens[t1].finish = i;
#endif
#if 1
    auto i = 0;
    auto t = 0;

    constexpr auto width = 4;
    char s[width];
    character_type type[width];
    bool delta[width - 1];

    for (; i < buffer_size; i += width - 1) {
        for (auto j = 0; j < width; j++) {
            s[j] = text[i+j];
        }

        for (auto j = 0; j < width; j++) {
            type[j] = types[s[j]];
        }

        for (auto j = 0; j < width - 1; j++) {
            delta[j] = type[j] != type[j+1];
            delta[j] |= s[j] == '{';
            delta[j] |= s[j] == '}';
            delta[j] |= s[j] == '(';
            delta[j] |= s[j] == ')';
            delta[j] |= s[j + 1] == '{';
            delta[j] |= s[j + 1] == '}';
            delta[j] |= s[j + 1] == '(';
            delta[j] |= s[j + 1] == ')';
        }

        for (auto j = 0; j < width - 1; j++) {
            auto d = 0;
            for (auto k = 0; k < j; ++k) { d += delta[k]; }
            tokens[t + d].finish = i + j + 1;
            tokens[t + d + 1].start = i + j + 1;
        }

        for (auto k = 0; k < width - 1; ++k) { t += delta[k]; }
    }
    tokens[t].finish = i;
    token_count = t + 1;
#endif
#if 0
    auto i = 0;
    auto t0 = 0;
    auto t1 = 1;
    auto t2 = 2;
    auto t3 = 3;

    constexpr auto width = 8;
    u8 delta = 0;

    for (auto finish = text + buffer_size; text < finish;) {
        delta = 0;
        for (auto j = 0; j < width; j++) {
            auto tj = types[text[j]];
            auto tn = types[text[j+1]];
            delta |= (tj != tn) << j;
        }

        auto t0t = types[text[0]];
        tokens[t0].type = t0t;
        tokens[t0].finish = i + dtable[delta].i1;
        tokens[t1].start = i + dtable[delta].i1;

        auto t1t = types[text[dtable[delta].i1 - 1]];
        tokens[t1].type = t1t;
        tokens[t1].finish = i + dtable[delta].i2;
        tokens[t2].start = i + dtable[delta].i2;

        auto t2t = types[text[dtable[delta].i2 - 1]];
        tokens[t2].type = t2t;
        tokens[t2].finish = i + dtable[delta].i3;
        tokens[t3].start = i + dtable[delta].i3;

        i += dtable[delta].i3;
        text += dtable[delta].i3;

        auto dt = dtable[delta].t;
        t0 += dt;
        t1 += dt;
        t2 += dt;
        t3 += dt;
    }
    token_count = t0;
    //tokens[t1].finish = i;
#endif

    for (auto j = 0; j < token_count; ++j) {
        auto start = tokens[j].start;
        auto finish = tokens[j].finish;
        auto str = std::string_view{ buffer + start, (size_t)finish - start };
        tokens[j].type = (token_type)types[str[0]];
        if (str == "if" || str == "for" || str == "ret") {
            tokens[j].type = token_type::keyword;
        }
        if (str == "(" || str == ")") {
            tokens[j].type = token_type::expression;
        }
        if (str == "import") {
            tokens[j].type = token_type::import;
        }
        if (str == "->") {
            tokens[j].type = token_type::arrow;
        }
    }
}

void expression() {
    flags = 0;
    expressions[eid].start = expressions[eid-1].finish;
    eid++;
}

auto token(int tid) {
    auto start = tokens[tid].start;
    auto finish = tokens[tid].finish;
    return std::string_view{ buffer + start, (size_t)finish - start };
}

struct {
    u32 mask;
    token_type type;
    std::string_view str;
} expectations;

void parse() {
    if (token_count < 2) return;

    struct frame {
        u32 start;
    };

    frame scopes[64];
    u32 sp = 0;

    if (token_count) {
        expressions[eid].start = 0;
        eid++;
    }

    enum {
        CHECK_ID = 0,
        CHECK_LPAREN = 1,
        CHECK_RPAREN = 2,
        CHECK_ARROW = 3,
        MATCH_CALL = 4,
        CHECK_IDENTIFIER = 5
    };

    bool new_scope = false;
    u32 wait = 0;
    for (; tid <= token_count; tid+=1) {
        expressions[eid - 1].finish = tid;
        expressions[eid - 1].next = eid;

        if (tid == token_count) break;

        if (tokens[tid].type == token_type::whitespace) continue;

        switch (flags) {
            case 0: flags += (tokens[tid].type == token_type::identifier); break;
            case 1: flags += (token(tid) == "("); break;
            case 2: flags += (token(tid) == ")"); break;
            case 3: flags += (token(tid) == "->") * 2;
                    flags += (token(tid) != "->"); break;
            case 4: break;
            case 5: flags += (tokens[tid].type == token_type::identifier); break;
            case 6: break;
        }

        std::cout << token(tid) << " " << flags << '\n';

        if (wait != 0) {
            if (wait == token(tid)[0]) {
                wait = 0;
            }
            continue;
        }

        if (flags == 4 || flags == 6) {
            expression();
        }

        if (token(tid) == "(") {
            wait = ')';
            continue;
        }
        if (token(tid) == "{") {
            scopes[sp].start = eid;
            sp++;
            expression();
            new_scope = true;
            continue;
        }
        if (token(tid) == "}") {
            sp--;
            expressions[scopes[sp].start].next = eid + 1;
            expression();
            new_scope = true;
            continue;
        }
        if (new_scope) {
            expression();
            new_scope = false;
            continue;
        }
        if (tokens[tid].type == token_type::keyword) {
            expression();
            continue;
        }
        if (tokens[tid-2].type == token_type::keyword) {
            expression();
            continue;
        }
        if (tokens[tid].type == tokens[tid-2].type) {
            expression();
            continue;
        }
    }
    expression_count = eid;
}
