#include <iostream>
#include <tuple>
#include <cassert>
#include <unordered_set>
#include <vector>
#include <string_view>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <string_view>
#include <algorithm>
#include <memory>
#include <thread>
#include <unordered_map>
#include <fcntl.h>
#include <unistd.h>
#include <bitset>
#include <ftl/container.hpp>
#include <ftl/benchmark.hpp>
#include <ftl/bits.hpp>
#include <ftl/args.hpp>

u32 offsets[4096];

struct {
    u32 start;
    u32 count;
}
