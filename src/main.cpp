#include <iostream>
#include <tuple>
#include <cassert>
#include <unordered_set>
#include <vector>
#include <string_view>
#include <sys/stat.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <string_view>
#include <algorithm>
#include <memory>
#include <thread>
#include <unordered_map>
#include <fcntl.h>
#include <unistd.h>
#include <bitset>
#include <ftl/container.hpp>
#include <ftl/benchmark.hpp>
#include <ftl/bits.hpp>
#include <ftl/args.hpp>

#include "lex.hpp"

using namespace std;

#define PRINT_EXPRESSIONS
//#define PRINT_TOKENS


int main(int argc, const char* argv[]) {
    using namespace ftl;
    auto time = std::chrono::microseconds{};

    option("-s", "--buffer-size", buffer_size);
    parse(argc, argv);

    for (auto& error : option_errors()) {
        std::cout << error << "\n";
    }

    if (option_errors().size()) {
        return 1;
    }

    if (args().size() == 1) {
        std::cout << "Usage: gt <file name>\n";
        return 1;
    }

    std::unique_ptr<char[]> buffer_object = std::unique_ptr<char[]>{ new char[buffer_size] };
    buffer = buffer_object.get();

#if 0
    auto file_name = (args[1] == "-") ? STDIN_FILENO : args[1];
    int fd = open(file_name.data(), O_RDONLY);
    buffer_size = read(fd, (char*)buffer, buffer_size);
#else
#define SAMPLE 1
    const char* samples[] = {
    R"(
            import io

            token = struct { type = u8 }
            token.type

            tokens = token[4096]

            read() -> void { buffer = "mock" }

            buffer = (char*) null

            if words < 2 {
                var = 1
                print(var)
            } else {
                var = 0
                print(var)
            }
    )",
    R"(
            token = struct { type = u8 }
            read() -> void {}
            buffer = (char*) null
            print(var)
            print(var)
    )",
    R"(
            tokens = token[4096]
            ret tokens[0]
    )",
    R"(
            token = struct { type = u8 }
            ret token.type
    )",
    R"(
            if words < 2 {
                ret 0
            } else {
                ret 1
            }
    )",
    R"(
            ret words < 2
    )"
    };
    buffer = samples[SAMPLE];
    buffer_size = strlen(buffer);
#endif
    {
        auto sample = time_sample{time};
        lex();
        parse();
    }

#ifdef PRINT_TOKENS
    for (auto i = 0; i < token_count; ++i) {
        auto size = tokens[i].finish - tokens[i].start;
        auto token = std::string_view{ &buffer[tokens[i].start], (size_t)size };
        std::cout << size << " '" << ((tokens[i].type != character_type::whitespace) ? token : " ") << "'" << (u32)tokens[i].type << "\n";
    }
#endif

#ifdef PRINT_EXPRESSIONS
    for (auto j = 0; j < expression_count; j = expressions[j].next) {
        auto& e = expressions[j];
        auto i = e.start;
        auto i1 = e.finish;

        for (; i < i1; ++i) {
            auto start = tokens[i].start;
            auto size = tokens[i].finish - start;
            auto token = std::string_view{ &buffer[start], (size_t)size };
            auto str = std::string_view{};
            str = (tokens[i].type == token_type::whitespace) ? " " : token;
            str = (buffer[tokens[i].start] == '{') ? "<scope>" : str;
            std::cout << str << ":" << (u32)tokens[i].type << " ";
        }
        std::cout << "\n";
    }
#endif

    std::cout << "Stream size: " << buffer_size << "\n";
    std::cout << "Time elapsed: " << time.count() << "us\n";

    return 0;
}
