#include <iostream>
#include <stack>
#include <string>
#include <random>
#include <unordered_map>
#include <ftl/args.hpp>

using namespace std;

static std::random_device entropy;
static std::mt19937 generator;

static int bytes = 0;
static int shift = 0;

struct Scope {
    Scope() { ++shift; }
    ~Scope() { --shift; }
};

void write(const std::string& line) {
    for (auto i = 0; i < shift; ++i) {
        std::cout << '\t';
    }
    bytes += line.size();
    std::cout << line << '\n';
}

auto rng(unsigned long start, unsigned long finish) -> long {
    std::uniform_int_distribution<> distribution(start, finish);
    return distribution(generator);
}

template <unsigned N>
auto take(const char* (&words)[N]) -> const char* {
    return words[rng(0, N-1)];
}

auto number() -> std::string {
    return std::to_string(rng(-5,19));
}

auto type() -> std::string {
    const char* types[] = {
        "int", "uint", "float", "string", "void", "bool", "u32"
    };
    return take(types);
}

auto identifier() -> std::string {
    const char* words[] = {
        "man", "jar", "skin", "books", "carriage", "card", "committee", "coat", "theory", "peace",
        "bulb", "reading", "glass", "grandfather", "observation", "cattle", "experience", "meeting", "surprise", "square",
        "mint", "steel", "star", "desire", "skirt", "foot", "amusement", "insect", "hammer", "ball",
        "canvas", "kettle", "toe", "yoke", "grain", "laborer", "woman", "brass", "morning", "rain"
    };
    return take(words);
}

auto comment() -> std::string  {
    std::string result = "// ";
    const char* words[] = {
        "lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit", "theory", "peace"
    };
    for (auto i = 0; i < rng(8,13); ++i) {
        result += " ";
        result += take(words);
    }
    return result;
}

auto operation() -> std::string {
    const char* operators[] = {
        "+", "-", "*", "/", "<<"
    };
    return take(operators);
}

auto expression() -> std::string {
    auto result = identifier();
    result += " = ";
    result += identifier();
    for (auto i = 0; i < rng(3,7); ++i) {
        result += " ";
        result += operation();
        result += " ";
        result += (i < 4) ? identifier() : number();
    }
    return result;
}

void line(const std::string& content) {
    if (rng(0,100) < 20) {
        write(comment());
    }
    write(content);
}

void statement() {
    if (rng(0,100) < 10) {
        auto result = std::string();
        result += "if (";
        result += expression();
        result += ") {";
        line(result);
        for (auto i = 0; i < rng(1,5); ++i) {
            auto cond = Scope{};
            line(expression());
        }
        write("}");
    } else if (rng(0,100) < 10) {
        auto result = std::string();
        result += "for ";
        result += identifier();
        result += " {";
        line(result);
        for (auto i = 0; i < rng(1,5); ++i) {
            auto cond = Scope{};
            line(expression());
        }
        write("}");
    } else {
        line(expression());
    }
}

void functional() {
    line(identifier() + "(" + ")" + " -> " + type() + " {");
    for (auto i = 0; i < rng(5,30); ++i) {
        auto fn = Scope{};
        statement();
    }
    auto result = std::string();
    result += "\tret ";
    line(result + identifier());
    write("}");
}

void structural() {
    line(identifier() + " = struct {");
    for (auto i = 0; i < rng(3,9); ++i) {
        auto st = Scope{};
        line(identifier() + " = " + type());
    }
    write("}");
}

void declaration() {
    for (auto i = 0; i < rng(1,3); ++i) {
        write("");
        structural();
    }

    for (auto i = 0; i < rng(3,8); ++i) {
        write("");
        functional();
    }
}

int main(int argc, const char* argv[]) {
    using namespace ftl;
    auto size = 4096;
    {
        option("-s", "--buffer-size", size);
        parse(argc, argv);
    }

    for (auto& e : option_errors()) {
        std::cout << "gtsrcgen: " << e << "\n";
    }

    if (option_errors().size()) {
        return 1;
    }

    generator.seed(entropy());
    while (bytes < size) {
        declaration();
    }
    return 0;
}
