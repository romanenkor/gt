#!/bin/sh

# Enter the project root
cd $(dirname $0)

export CMAKE_PREFIX_PATH=$PWD
export CMAKE_CONFIG_TYPE=${CMAKE_CONFIG_TYPE:-Debug}

mkdir -p .build; cd .build

format=" %-20s %s\n"
spacer="printf '%.0s-' {1..50}; echo"

eval ${spacer}
printf "${format}" "CXX =" "${CXX:-< Not Set >}"
printf "${format}" "CC =" "${CC:-< Not Set >}"
printf "${format}" "CMAKE_PREFIX_PATH =" $CMAKE_PREFIX_PATH
printf "${format}" "CMAKE_CONFIG_TYPE =" $CMAKE_CONFIG_TYPE
eval ${spacer}

cmake -G "Unix Makefiles" -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. -DCMAKE_BUILD_TYPE=$CMAKE_CONFIG_TYPE || exit
cmake --build . -j || exit
